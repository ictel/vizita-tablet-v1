package id.co.transisimedia.vizitatablet.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import id.co.transisimedia.vizitatablet.databinding.DialogEditQtyBinding

/**
 * Created by fizhu on 24,May,2020
 * Hvyz.anbiya@gmail.com
 */
class QtyDialog(context: Context, private val onSaveClicked: (dialog: QtyDialog, qty: Int)-> Unit): Dialog(context) {

    private lateinit var binding: DialogEditQtyBinding
    private var quantity = 0
    private var stock = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogEditQtyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        window!!.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
        }
        onInit()
    }

    private fun onInit() {
        initView()
    }

    fun setStock(stock: Int) {
        this.stock = stock
    }

    fun setQty(quantity: Int) {
        this.quantity = quantity
    }

    private fun initView() {
        binding.btnPlus.setOnClickListener {
            if (quantity < stock) {
                quantity += 1
                binding.tvQuantity.text = quantity.toString()
            }
        }
        binding.btnMinus.setOnClickListener {
            if (quantity > 1) {
                quantity -= 1
                binding.tvQuantity.text = quantity.toString()
            }
        }
        binding.tvQuantity.text = quantity.toString()

        binding.btnOke.setOnClickListener {
            onSaveClicked.invoke(this, quantity)
            this.dismiss()
        }
    }


    override fun onBackPressed() {
        //do nothing
    }

}