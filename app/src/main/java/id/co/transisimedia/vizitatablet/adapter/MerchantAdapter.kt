package id.co.transisimedia.vizitatablet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.co.transisimedia.vizitatablet.R
import id.co.transisimedia.vizitatablet.data.models.Merchant
import id.co.transisimedia.vizitatablet.databinding.ItemListMerchantBinding

/**
 * Created by fizhu on 12,June,2020
 * Hvyz.anbiya@gmail.com
 */

class MerchantAdapter(
        private val list: List<Merchant>,
        private val context: Context,
        private val callBack: (Merchant) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var binding: ItemListMerchantBinding? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = list[position]

        binding?.tvMerchantName?.text = data.name
        binding?.tvStatus?.text = data.operational?.status
        binding?.tvDesc?.text = data.description

        Glide.with(context)
                .load(data.file)
                .placeholder(R.drawable.icon)
                .error(R.drawable.icon)
                .into(binding?.ivMerchant!!)

        binding?.root?.setOnClickListener { callBack.invoke(data) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = ItemListMerchantBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding?.root!!)
    }

    override fun getItemCount(): Int = list.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}