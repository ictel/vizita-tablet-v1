package id.co.transisimedia.vizitatablet.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.co.transisimedia.vizitatablet.MainActivity
import id.co.transisimedia.vizitatablet.R
import id.co.transisimedia.vizitatablet.adapter.MenuAdapter
import id.co.transisimedia.vizitatablet.data.api.Api
import id.co.transisimedia.vizitatablet.data.db.DataSource
import id.co.transisimedia.vizitatablet.data.models.Menu
import id.co.transisimedia.vizitatablet.databinding.FragmentMenuBinding
import id.co.transisimedia.vizitatablet.utils.DataSession
import id.co.transisimedia.vizitatablet.utils.loge
import id.co.transisimedia.vizitatablet.utils.route
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable


/**
 * Created by fizhu on 12,June,2020
 * Hvyz.anbiya@gmail.com
 */
class MenuFragment : Fragment() {

    private var binding: FragmentMenuBinding? = null
    private val compositeDisposable by lazy { CompositeDisposable() }
    private val dataSource by lazy { DataSource() }
    private var currentPage = 1
    private val listMenu: Observable<List<Menu>>
        get() = dataSource.listMenu
    private var typeTax = 0
    private var taxMerchant = 0
    private var merchantId = 0
    private lateinit var menuAdapter: MenuAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMenuBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInit()
    }

    private fun onInit() {
        binding?.btnClose?.setOnClickListener { (activity as MainActivity).popBackStackFragment() }
        handleBackpressed()
        merchantId = arguments?.getInt("id")?:0
        initRv()
        getListMerchant(merchantId)
    }

    private fun getListMenuFromDb() {
        compositeDisposable.route(listMenu,
                main = {
                    if (menuAdapter == null) {
                        initRv()
                        menuAdapter.setData(it)
                    } else {
                        menuAdapter.setData(it)
                    }
                },
                error = {
                    loge(it.localizedMessage)
                }
        )
    }

    private fun initRv() {
        menuAdapter = MenuAdapter(
                context = requireContext(),
                onPlusClicked = { id, quantity ->
                    dataSource.updateQuantity(id, quantity)
                },
                onMinusClicked = { id, quantity ->
                    dataSource.updateQuantity(id, quantity)
                }
        )
        with(binding?.rv!!) {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            adapter = menuAdapter
        }
        DataSession.keyTypeTax = typeTax
        DataSession.keyTaxMerchant = taxMerchant
        DataSession.keyMerchantId = merchantId
        loge("ID : $merchantId")
        binding?.fabCart?.setOnClickListener { (activity as MainActivity).initCart() }
    }

    private fun getListMerchant(id: Int) {
        show(View.GONE, View.VISIBLE, View.GONE)
        compositeDisposable.route(
                Api.create().getDetailMerchant(id, currentPage.toString()),
                main = {
                    if (it.status) {
                        show(View.VISIBLE, View.GONE, View.GONE)
                        if (it.data?.merchant != null) {
                            show(View.VISIBLE, View.GONE, View.GONE)
                            binding?.tvMerchantName?.text = it.data.merchant.name
                            binding?.tvStatus?.text = it.data.merchant.operational?.status
                            binding?.tvDesc?.text = it.data.merchant.description
                            binding?.tvLocation?.text = it.data.merchant.address
                            typeTax = it.data.merchant.revenue_sharing_type ?: 0
                            taxMerchant = it.data.merchant.revenue_sharing ?: 0
                            merchantId = it.data.merchant.id?:0
                            DataSession.keyTypeTax = typeTax
                            DataSession.keyTaxMerchant = taxMerchant
                            DataSession.keyMerchantId = merchantId

                            loge("TAX MERCH : $taxMerchant , TYPE : $typeTax")
                            Glide.with(requireContext())
                                    .load(it.data.merchant.file)
                                    .placeholder(R.drawable.icon)
                                    .error(R.drawable.icon)
                                    .into(binding?.ivMerchant!!)
                            if (it.data.list_menu.isNotEmpty()) {
                                dataSource.insertAll(it.data.list_menu)
                                getListMenuFromDb()
                            }
                        } else {
                            show(View.GONE, View.GONE, View.VISIBLE)
                        }
                    } else {
                        show(View.GONE, View.GONE, View.VISIBLE)
                    }
                },
                error = {
                    show(View.GONE, View.GONE, View.VISIBLE)
                    loge(it.localizedMessage)
                }
        )
    }

    private fun show(rv: Int, pb: Int, err: Int) {
        binding?.layoutDetail?.visibility = rv
        binding?.pb?.visibility = pb
        binding?.tvNodata?.visibility = err
    }

    private fun handleBackpressed() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity).popBackStackFragment()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }
}