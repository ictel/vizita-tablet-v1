package id.co.transisimedia.vizitatablet.utils

import id.co.transisimedia.vizitatablet.App


/**
 * Created by fizhu on 22,May,2020
 * Hvyz.anbiya@gmail.com
 */
object DataSession {

    private const val KEY_ID_KARYAWAN = "Pref.keyIdKaryawan"
    private const val KEY_TYPE_TAX = "Pref.keyTypeTax"
    private const val KEY_TAX_MERCHANT = "Pref.keyTaxMerchant"
    private const val KEY_MERCHANT_ID = "Pref.keyMerchantId"
    private const val KEY_ID_RESERVASI = "Pref.keyIdReservasi"

    var keyIdKaryawan: String?
        get() = App.spData.getString(KEY_ID_KARYAWAN, "")
        set(data) = App.spData.edit().putString(KEY_ID_KARYAWAN, data).apply()

    var keyIdReservasi: String?
        get() = App.spData.getString(KEY_ID_RESERVASI, "")
        set(data) = App.spData.edit().putString(KEY_ID_RESERVASI, data ?: "").apply()

    var keyTypeTax: Int?
        get() = App.spData.getInt(KEY_TYPE_TAX, 0)
        set(data) = App.spData.edit().putInt(KEY_TYPE_TAX, data ?: 0).apply()

    var keyTaxMerchant: Int?
        get() = App.spData.getInt(KEY_TAX_MERCHANT, 0)
        set(data) = App.spData.edit().putInt(KEY_TAX_MERCHANT, data ?: 0).apply()

    var keyMerchantId: Int?
        get() = App.spData.getInt(KEY_MERCHANT_ID, 0)
        set(data) = App.spData.edit().putInt(KEY_MERCHANT_ID, data ?: 0).apply()

    fun clear() {
        keyTypeTax = 0
        keyTaxMerchant = 0
        keyMerchantId = 0
    }

}