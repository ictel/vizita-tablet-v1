package id.co.transisimedia.vizitatablet

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import id.co.transisimedia.vizitatablet.data.db.Db

/**
 * Created by fizhu on 13,June,2020
 * Hvyz.anbiya@gmail.com
 */
class App: Application() {

    lateinit var db: Db

    override fun onCreate() {
        super.onCreate()
        context = this
        singleton = this
        db = Db.getInstance(this)
    }

    companion object {

        @SuppressLint("StaticFieldLeak")
                /**
                 * The application [Context] made static.
                 * Do **NOT** use this as the context for a view,
                 * this is mostly used to simplify calling of resources
                 * (esp. String resources) outside activities.
                 */
        var context: Context? = null
            private set

        @SuppressLint("StaticFieldLeak")
        var singleton: App? = null
            private set

        val getInstance: App?
            get() = singleton

        val spData: SharedPreferences
            get() = getInstance!!.getSharedPreferences("myDataPref", Context.MODE_PRIVATE)

    }

}