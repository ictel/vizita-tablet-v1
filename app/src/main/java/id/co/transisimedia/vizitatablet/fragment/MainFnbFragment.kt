package id.co.transisimedia.vizitatablet.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.barcode.Barcode
import id.co.transisimedia.vizitatablet.BarcodeCaptureActivity
import id.co.transisimedia.vizitatablet.MainActivity
import id.co.transisimedia.vizitatablet.data.api.Api
import id.co.transisimedia.vizitatablet.databinding.FragmentMainFnbBinding
import id.co.transisimedia.vizitatablet.dialog.AuthDialog
import id.co.transisimedia.vizitatablet.utils.*
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by fizhu on 08,July,2020
 * https://github.com/Fizhu
 */
class MainFnbFragment: Fragment() {

    private var binding: FragmentMainFnbBinding? = null
    private val compositeDisposable by lazy { CompositeDisposable() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMainFnbBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInit()
    }

    private fun onInit() {
        binding?.btnClose?.setOnClickListener { (activity as MainActivity).backFromFNB() }
        handleBackpressed()
        onClick()
    }

    private fun onClick() {
        binding?.cardInternal?.setOnClickListener {
            authWithMerch(DataSession.keyIdReservasi?:"")
        }
        binding?.cardExternal?.setOnClickListener {
            if (!requireActivity().isFinishing) {
                val dialog = AuthDialog(requireContext(),
                        onButtonClicked = { dialog, code ->
                            (activity as MainActivity).goFullscreen()
                            auth(code)
                            dialog.dismiss()
                        }, onQRClicked = { dialog ->
                    (activity as MainActivity).goFullscreen()
                    scanBarcode()
                    dialog.dismiss()
                }, onBackClicked = { dialog ->
                    (activity as MainActivity).goFullscreen()
                    dialog.dismiss()
                })
                dialog.setCancelable(false)
                dialog.show()
            }
        }
    }

    private fun auth(code: String) {
        val x = HashMap<String, String>()
        x["code"] = code
        showProgressDialog(requireContext(), "Loading...")
        DataSession.clear()
        compositeDisposable.route(
                Api.create().authMerchant(x),
                main = {
                    if (it.status) {
                        if (it.data != null) {
                            dismissProgressDialog()
                            (activity as MainActivity).goFullscreen()
                            DataSession.keyIdKaryawan = it.data.id_karyawan
                            next(2)
                        } else {
                            dismissProgressDialog()
                            Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        dismissProgressDialog()
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }
                },
                error = {
                    dismissProgressDialog()
                    Toast.makeText(requireContext(), "Lost connection, your request cannot be proceed", Toast.LENGTH_SHORT).show()
                    loge(it.localizedMessage)
                }
        )
    }

    private fun authWithMerch(idReservasi: String) {
        val x = HashMap<String, String>()
        x["id_reservasi"] = idReservasi
        showProgressDialog(requireContext(), "Loading...")
        DataSession.clear()
        compositeDisposable.route(
                Api.create().authMerchantReservasi(x),
                main = {
                    if (it.status) {
                        if (it.data != null) {
                            dismissProgressDialog()
                            (activity as MainActivity).goFullscreen()
                            DataSession.keyIdKaryawan = it.data.id_karyawan
                            next(1)
                        } else {
                            dismissProgressDialog()
                            Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        dismissProgressDialog()
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }
                },
                error = {
                    dismissProgressDialog()
                    Toast.makeText(requireContext(), "Lost connection, your request cannot be proceed", Toast.LENGTH_SHORT).show()
                    loge(it.localizedMessage)
                }
        )
    }

    private fun scanBarcode() {
        val intent = Intent(requireActivity(), BarcodeCaptureActivity::class.java)
        intent.putExtra(BarcodeCaptureActivity.AutoFocus, true)
        intent.putExtra(BarcodeCaptureActivity.UseFlash, false)

        startActivityForResult(intent, MainActivity.RC_BARCODE_CAPTURE_FNB)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == MainActivity.RC_BARCODE_CAPTURE_FNB) {

            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    val barcode: Barcode? = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject)
                    if (barcode != null) {
                        auth(barcode.displayValue)
                    } else {
                        Toast.makeText(context, "No QR Code Detected", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    //statusMessage.setText(R.string.barcode_failure);
                    Log.d("VIZITA", "No QR Code captured")
                }
            } else {
                Toast.makeText(context, "No QR Code Detected", Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun next(int: Int) {
        val bundle = Bundle()
        bundle.putInt("merchType", int)
        (activity as MainActivity).initFnb(bundle)
    }

    private fun handleBackpressed() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity).backFromFNB()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }
}