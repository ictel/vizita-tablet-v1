package id.co.transisimedia.vizitatablet.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.co.transisimedia.vizitatablet.R
import id.co.transisimedia.vizitatablet.data.models.Menu
import id.co.transisimedia.vizitatablet.databinding.ItemListCartBinding
import id.co.transisimedia.vizitatablet.databinding.ItemListMenuBinding
import id.co.transisimedia.vizitatablet.utils.loge

/**
 * Created by fizhu on 12,June,2020
 * Hvyz.anbiya@gmail.com
 */

class CartAdapter(
        private val list: List<Menu>,
        private val context: Context,
        private val onEditClicked: (id: String, note: String) -> Unit,
        private val onEditQtyClicked: (id: String, stock: Int, quantity: Int) -> Unit,
        private val onDeleteClicked: (id: String) -> Unit
) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemListCartBinding.inflate(LayoutInflater.from(context), parent, false).root)
    }

    override fun getItemCount(): Int = list.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemListCartBinding.bind(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]
        with(holder.binding) {
            tvName.text = data.name
            tvDesc.text = data.description

            val price = data.price as String

            if (price != "0") {
               tvPrice.text = "Rp. ${data.price}"
            } else {
                tvPrice.text = "Free"
            }

            Glide.with(context)
                    .load(data.file)
                    .placeholder(R.drawable.icon)
                    .error(R.drawable.icon)
                    .into(ivMenu)

            val quantity: Int = data.quantity ?: 0

            tvQuantity.text = "$quantity x"

            tvEditQty.setOnClickListener {
                onEditQtyClicked.invoke(data.id, data.stock ?: 0, quantity)
            }

            btnEdit.setOnClickListener {
                onEditClicked.invoke(data.id, data.note ?: "")
            }
            btnDelete.setOnClickListener { onDeleteClicked.invoke(data.id) }

            loge(data.note)

            if (data.note != "" || data.note.isNotEmpty() || data.note.isNotBlank()) {
                if (data.note != null) {
                    btnEdit.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_check_circle_24))
                }
            } else {
                btnEdit.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_outline_edit_24))
            }
        }
    }
}