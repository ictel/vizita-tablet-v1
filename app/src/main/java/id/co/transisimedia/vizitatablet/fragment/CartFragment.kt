package id.co.transisimedia.vizitatablet.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.JsonParser
import id.co.transisimedia.vizitatablet.MainActivity
import id.co.transisimedia.vizitatablet.R
import id.co.transisimedia.vizitatablet.adapter.CartAdapter
import id.co.transisimedia.vizitatablet.data.api.Api
import id.co.transisimedia.vizitatablet.data.db.DataSource
import id.co.transisimedia.vizitatablet.data.models.Menu
import id.co.transisimedia.vizitatablet.data.models.Order
import id.co.transisimedia.vizitatablet.data.models.OrderProduct
import id.co.transisimedia.vizitatablet.databinding.FragmentCartBinding
import id.co.transisimedia.vizitatablet.dialog.NoteDialog
import id.co.transisimedia.vizitatablet.dialog.QtyDialog
import id.co.transisimedia.vizitatablet.utils.*
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.util.*

/**
 * Created by fizhu on 12,June,2020
 * Hvyz.anbiya@gmail.com
 */
class CartFragment : Fragment() {

    private var binding: FragmentCartBinding? = null
    private val compositeDisposable by lazy { CompositeDisposable() }
    private val dataSource by lazy { DataSource() }
    private val listCart: Observable<List<Menu>>
        get() = dataSource.listCart
    private var total = 0
    private var subtotal = 0
    private var tax = 0
    private var taxType = 0
    private var taxMerchant = 0
    private var merchantId = 0
    private var reservationId: String? = null
    private var idKaryawan: String? = null
    private var order: Order? = null
    private var listOrderProduct: MutableList<OrderProduct> = mutableListOf()
    private lateinit var regId: HashMap<String, String>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInit()
    }

    private fun onInit() {
        reservationId = DataSession.keyIdReservasi
        idKaryawan = DataSession.keyIdKaryawan
        regId = HashMap<String, String>()
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            regId["reg_id"] = it.token
        }
        handleBackpressed()
        taxType = DataSession.keyTypeTax ?: 0
        taxMerchant = DataSession.keyTaxMerchant ?: 0
        merchantId = DataSession.keyMerchantId ?: 0
        binding?.btnAdd?.setOnClickListener { (activity as MainActivity).popBackStackFragment() }

        loge("TAX MERCH : $taxMerchant , TYPE : $taxType")

        binding?.btnOrder?.setOnClickListener {
            if (idKaryawan != null || idKaryawan != "") {
                submitOrder()
            }
        }
        getListCartFromDb()
    }

    private fun submitOrder() {
        val gson = Gson()
        val x = JsonParser().parse(gson.toJson(order)).asJsonObject
        showProgressDialog(requireContext(), "Loading...")
        compositeDisposable.route(
                Api.create().order(x),
                main = {
                    if (it.status) {
                        dismissProgressDialog()
                        if (it.data != null) {
                            val bundle = Bundle()
                            bundle.putInt("id", it.data.id ?: 0)
                            (activity as MainActivity).initOrder(bundle)
                        } else {
                            dismissProgressDialog()
                            Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        dismissProgressDialog()
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }
                },
                error = {
                    dismissProgressDialog()
                    Toast.makeText(requireContext(), "Lost connection, your request cannot be proceed", Toast.LENGTH_SHORT).show()
                    loge(it.localizedMessage)
                }
        )
    }

    override fun onResume() {
        super.onResume()
        taxType = DataSession.keyTypeTax ?: 0
        taxMerchant = DataSession.keyTaxMerchant ?: 0
        getListCartFromDb()
    }

    private fun setText() {
        total = subtotal + tax
        binding?.tvSubTotal?.text = subtotal.toString()
        binding?.tvTax?.text = tax.toString()
        binding?.tvTotal?.text = total.toString()
    }

    private fun getListCartFromDb() {
        show(View.GONE, View.VISIBLE, View.GONE)
        compositeDisposable.route(listCart,
                main = {
                    listOrderProduct.clear()
                    if (it.isNotEmpty()) {
                        it.forEach { menu ->
                            listOrderProduct.add(
                                    OrderProduct(
                                            id = menu.id.toInt(),
                                            quantity = menu.quantity,
                                            price = menu.price?.toInt(),
                                            note = menu.note ?: ""
                                    )
                            )
                        }
                        reset()
                        if (listOrderProduct.size > 0) {
                            listOrderProduct.forEach { orderProduct ->
                                val q = orderProduct.quantity ?: 0
                                val p = orderProduct.price ?: 0
                                subtotal += (p * q)
                                tax = when (taxType) {
                                    1 -> subtotal * taxMerchant / 100
                                    else -> taxMerchant
                                }
                                if (subtotal == 0) {
                                    tax = 0
                                }
                            }
                        } else {
                            reset()
                            setText()
                        }

                        //order
                        order = null
                        order = Order(
                                fcm = regId,
                                id_karyawan = idKaryawan,
                                id_merchant = merchantId,
                                total_price = total,
                                tax = tax,
                                id_reservasi = reservationId,
                                order_product = listOrderProduct.toList()
                        )
                        initRv(it)
                        show(View.VISIBLE, View.GONE, View.GONE)
                        binding?.btnOrder?.isEnabled = true
                        binding?.btnOrder?.setBackgroundResource(R.drawable.rounded_green)
                        setText()
                    } else {
                        reset()
                        setText()
                        show(View.GONE, View.GONE, View.VISIBLE)
                        binding?.btnOrder?.isEnabled = false
                        binding?.btnOrder?.setBackgroundResource(R.drawable.rounded_grey)
                    }
                },
                error = {
                    reset()
                    setText()
                    show(View.GONE, View.GONE, View.VISIBLE)
                    binding?.btnOrder?.isEnabled = false
                    binding?.btnOrder?.setBackgroundResource(R.drawable.rounded_grey)
                    loge(it.localizedMessage)
                }
        )
    }

    private fun reset() {
        subtotal = 0
        tax = 0
    }

    private fun initRv(list: List<Menu>) {
        val adapterCart =
                CartAdapter(
                        list = list,
                        context = requireContext(),
                        onEditClicked = { id, note ->
                            if (!requireActivity().isFinishing) {
                                val dialog = NoteDialog(requireContext(), onSaveClicked = { dialog, note1 ->
                                    dataSource.updateNote(id, note1)
                                    (activity as MainActivity).goFullscreen()
                                    dialog.dismiss()
                                })
                                dialog.setNote(note)
                                dialog.setCancelable(false)
                                dialog.show()
                            }
                        },
                        onDeleteClicked = { id ->
                            dataSource.updateQuantity(id, 0)
                        },
                        onEditQtyClicked = { id, stock, quantity ->
                            if (!requireActivity().isFinishing) {
                                val dialog = QtyDialog(requireContext(), onSaveClicked = { dialog, qty ->
                                    dataSource.updateQuantity(id, qty)
                                    (activity as MainActivity).goFullscreen()
                                    dialog.dismiss()
                                })
                                dialog.setStock(stock)
                                dialog.setQty(quantity)
                                dialog.setCancelable(false)
                                dialog.show()
                            }
                        }
                )
        with(binding?.rv!!) {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            adapter = adapterCart
        }
        adapterCart.notifyDataSetChanged()
    }

    private fun show(rv: Int, pb: Int, err: Int) {
        binding?.rv?.visibility = rv
        binding?.pb?.visibility = pb
        binding?.tvNodata?.visibility = err
    }

    private fun handleBackpressed() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity).popBackStackFragment()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }

}