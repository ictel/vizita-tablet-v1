package id.co.transisimedia.vizitatablet.data.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by fizhu on 10,June,2020
 * Hvyz.anbiya@gmail.com
 */

data class Merchant(
        @Expose @SerializedName("id")
        val id: Int? = 0,
        @Expose @SerializedName("id_type_merchant")
        val id_type_merchant: String? = null,
        @Expose @SerializedName("name")
        val name: String? = null,
        @Expose @SerializedName("address")
        val address: String? = null,
        @Expose @SerializedName("description")
        val description: String? = null,
        @Expose @SerializedName("phone_number")
        val phone_number: String? = null,
        @Expose @SerializedName("email")
        val email: String? = null,
        @Expose @SerializedName("revenue_sharing")
        val revenue_sharing: Int? = null,
        @Expose @SerializedName("revenue_sharing_type")
        val revenue_sharing_type: Int? = null,
        @Expose @SerializedName("prepare_time")
        val prepare_time: String? = null,
        @Expose @SerializedName("deliver_time")
        val deliver_time: String? = null,
        @Expose @SerializedName("file")
        val file: String? = null,
        @Expose @SerializedName("operational")
        val operational: Operational? = null
)

data class NewMerchant(
        @Expose @SerializedName("id")
        val id: Int? = 0,
        @Expose @SerializedName("id_type_merchant")
        val id_type_merchant: String? = null,
        @Expose @SerializedName("name")
        val name: String? = null,
        @Expose @SerializedName("address")
        val address: String? = null,
        @Expose @SerializedName("description")
        val description: String? = null,
        @Expose @SerializedName("phone_number")
        val phone_number: String? = null,
        @Expose @SerializedName("email")
        val email: String? = null,
        @Expose @SerializedName("revenue_sharing")
        val revenue_sharing: Int? = null,
        @Expose @SerializedName("revenue_sharing_type")
        val revenue_sharing_type: Int? = null,
        @Expose @SerializedName("prepare_time")
        val prepare_time: Int? = null,
        @Expose @SerializedName("deliver_time")
        val deliver_time: Int? = null,
        @Expose @SerializedName("file")
        val file: String? = null,
        @Expose @SerializedName("operational")
        val operational: Operational? = null
)

data class Operational(
        @Expose @SerializedName("status")
        val status: String? = null,
        @Expose @SerializedName("from")
        val from: String? = null,
        @Expose @SerializedName("to")
        val to: String? = null
)

data class DetailMerchant(
        @Expose @SerializedName("totalData")
        val totalData: String? = null,
        @Expose @SerializedName("merchant")
        val merchant: Merchant? = null,
        @Expose @SerializedName("list_menu")
        val list_menu: ArrayList<Menu>,
        @Expose @SerializedName("tax")
        val tax: String? = null
)

data class ListRoom (
        @SerializedName("id_ruang")
        @Expose
        val id_ruang: Int?,
        @SerializedName("nama_ruang")
        @Expose
        val nama_ruang: String?,
        @SerializedName("status_ruang")
        @Expose
        val status_ruang: String?
)

@Entity(tableName = "menu_table")
data class Menu(
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "id")
        @Expose @SerializedName("id")
        val id: String,
        @Expose @SerializedName("name")
        val name: String? = null,
        @Expose @SerializedName("description")
        val description: String? = null,
        @Expose @SerializedName("price")
        val price: String? = null,
        @Expose @SerializedName("is_promo")
        val is_promo: String? = null,
        @Expose @SerializedName("price_promo")
        val price_promo: String? = null,
        @Expose @SerializedName("stock")
        val stock: Int? = null,
        @Expose @SerializedName("file")
        val file: String? = null,
        @Expose @SerializedName("category")
        val category: String? = null,
        @Expose @SerializedName("quantity")
        var quantity: Int? = 0,
        @Expose @SerializedName("note")
        val note: String? = null
)

data class Order(
        @Expose @SerializedName("fcm")
        val fcm: HashMap<String, String>? = null,
        @Expose @SerializedName("id_karyawan")
        val id_karyawan: String? = null,
        @Expose @SerializedName("id_merchant")
        val id_merchant: Int? = 0,
        @Expose @SerializedName("total_price")
        val total_price: Int? = 0,
        @Expose @SerializedName("tax")
        val tax: Int? = 0,
        @Expose @SerializedName("id_reservasi")
        val id_reservasi: String? = null,
        @Expose @SerializedName("order_product")
        val order_product: List<OrderProduct>? = null
)

data class OrderProduct(
        @Expose @SerializedName("id")
        val id: Int? = 0,
        @Expose @SerializedName("quantity")
        val quantity: Int? = 0,
        @Expose @SerializedName("price")
        val price: Int? = 0,
        @Expose @SerializedName("note")
        val note: String? = null
)

@Parcelize
data class Karyawan(
        @Expose @SerializedName("id_karyawan")
        val id_karyawan: String? = null,
        @Expose @SerializedName("level")
        val level: String? = null,
        @Expose @SerializedName("id_department")
        val id_department: String? = null,
        @Expose @SerializedName("nama_lengkap")
        val nama_lengkap: String? = null,
        @Expose @SerializedName("jenis_kelamin")
        val jenis_kelamin: String? = null,
        @Expose @SerializedName("tanggal_lahir")
        val tanggal_lahir: String? = null,
        @Expose @SerializedName("status_kawin")
        val status_kawin: String? = null,
        @Expose @SerializedName("pendidikan")
        val pendidikan: String? = null,
        @Expose @SerializedName("nomor_ktp")
        val nomor_ktp: String? = null,
        @Expose @SerializedName("nomor_telepon")
        val nomor_telepon: String? = null,
        @Expose @SerializedName("email")
        val email: String? = null,
        @Expose @SerializedName("alamat")
        val alamat: String? = null,
        @Expose @SerializedName("nama_departemen")
        val nama_departemen: String? = null
) : Parcelable

@Parcelize
data class FcmData(
        @Expose @SerializedName("order")
        val order: FcmOrder? = null,
        @Expose @SerializedName("merchant")
        val merchant: FcmMerch? = null
) : Parcelable

@Parcelize
data class FcmOrder(
        @Expose @SerializedName("id")
        val id: Int? = 0,
        @Expose @SerializedName("status")
        val status: Int? = 0
) : Parcelable

@Parcelize
data class FcmMerch(
        @Expose @SerializedName("prepare_time")
        val prepare_time: Int? = 0,
        @Expose @SerializedName("deliver_time")
        val deliver_time: Int? = 0
) : Parcelable

data class OrderData(
        @Expose @SerializedName("id")
        val id: Int? = 0,
        @Expose @SerializedName("transaction_id")
        val transaction_id: String? = null,
        @Expose @SerializedName("total_price")
        val total_price: String? = null
)

data class DetailOrderData(
        @Expose @SerializedName("id")
        val id: Int? = 0,
        @Expose @SerializedName("transaction_id")
        val transaction_id: String? = null,
        @Expose @SerializedName("total_price")
        val total_price: String? = null,
        @Expose @SerializedName("id_status")
        val idStatus: Int? = 0,
        @Expose @SerializedName("merchant")
        val merchant: NewMerchant? = null
)