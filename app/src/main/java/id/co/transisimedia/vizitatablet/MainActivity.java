package id.co.transisimedia.vizitatablet;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.co.transisimedia.vizitatablet.databinding.ActivityMainBinding;
import id.co.transisimedia.vizitatablet.fragment.CartFragment;
import id.co.transisimedia.vizitatablet.fragment.FnbFragment;
import id.co.transisimedia.vizitatablet.fragment.HelpFragment;
import id.co.transisimedia.vizitatablet.fragment.MainFnbFragment;
import id.co.transisimedia.vizitatablet.fragment.MenuFragment;
import id.co.transisimedia.vizitatablet.fragment.OrderFragment;
import id.co.transisimedia.vizitatablet.libs.internetData;
import id.co.transisimedia.vizitatablet.utils.DataSession;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity {

    Context context = this;
    RelativeLayout container;
    SharedPreferences session;
    String ruang;
    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final int RC_BARCODE_CAPTURE_END = 9002;
    boolean check_availability = true;
    String is_login = "N";
    private FrameLayout frameLayout;
    private ActivityMainBinding activityMainBinding;
    public static final int RC_BARCODE_CAPTURE_FNB = 5566;

    private Spinner sp;
    private Integer idRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(activityMainBinding.getRoot());
        getListRoom();

        setFNBClicked();
        frameLayout = findViewById(R.id.container_main);
        session = getSharedPreferences("session", MODE_PRIVATE);
        ruang = session.getString("id_ruang", "");

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        if (session.getString("id_ruang", "").equals("")) {

            resetForm();
            showForm(R.id.form_setting);

            LinearLayout l1 = (LinearLayout) findViewById(R.id.enter_pin);
            l1.setVisibility(View.GONE);

            LinearLayout l2 = (LinearLayout) findViewById(R.id.setting_main);
            l2.setVisibility(View.VISIBLE);

        } else {

            if (!is_login()) {
                resetForm();
                showForm(R.id.form_login);
            } else {
                new tabletStatus(session.getString("id_ruang", "")).execute("");
            }

            final Handler h = new Handler();
            h.postDelayed(new Runnable() {
                private long time = 0;

                @Override
                public void run() {

                    Calendar rightNow = Calendar.getInstance();

                    String e = session.getString("est_time", "");

                    if (!e.equals("")) {

                        String[] est = e.split(":");
                        int current_hour = rightNow.get(Calendar.HOUR_OF_DAY);
                        int current_resv = Integer.parseInt(est[0]);

                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                        String current_time = sdf.format(rightNow.getTime());
                        String reserve_time = e;

                        String remaining = time_diff(current_time, reserve_time);
                        String[] rem = remaining.split(":");

//                        Log.d("current : ", Integer.toString(current_hour));
//                        Log.d("current_resv : ", Integer.toString(current_resv));
//                        Log.d("remaining : ", remaining);

                        if (rem[1] != null) {

                            if (rem[0].equals("00")) {

                                if (Integer.parseInt(rem[1]) <= 15 && Integer.parseInt(rem[1]) >= 10) {
                                    // disini buat yang 10 menit 5 menit
                                    TextView txt_habis = (TextView) findViewById(R.id.habis);
                                    txt_habis.setText("Your reservation is about to end in less than 15 minutes");
                                    txt_habis.setVisibility(View.VISIBLE);
                                } else if (Integer.parseInt(rem[1]) <= 10 && Integer.parseInt(rem[1]) >= 6) {
                                    // disini buat yang 10 menit 5 menit
                                    TextView txt_habis = (TextView) findViewById(R.id.habis);
                                    txt_habis.setText("Your reservation is about to end in less than 10 minutes");
                                    txt_habis.setVisibility(View.VISIBLE);
                                } else if (Integer.parseInt(rem[1]) <= 5 && Integer.parseInt(rem[1]) >= 1) {
                                    // disini buat yang 10 menit 5 menit
                                    TextView txt_habis = (TextView) findViewById(R.id.habis);
                                    txt_habis.setText("Your reservation is about to end in less than 5 minutes");
                                    txt_habis.setVisibility(View.VISIBLE);
                                } else if (Integer.parseInt(rem[1]) <= 1) {
                                    // disini buat yang 10 menit 5 menit
                                    TextView txt_habis = (TextView) findViewById(R.id.habis);
                                    txt_habis.setText("Your reservation time is up. Click end session bellow to end your reservation.");
                                    txt_habis.setVisibility(View.VISIBLE);
                                } else {
                                    TextView txt_habis = (TextView) findViewById(R.id.habis);
                                    txt_habis.setVisibility(View.GONE);
                                }

                            } else {
                                TextView txt_habis = (TextView) findViewById(R.id.habis);
                                txt_habis.setVisibility(View.GONE);
                            }

                        } else {
                            TextView txt_habis = (TextView) findViewById(R.id.habis);
                            txt_habis.setVisibility(View.GONE);
                        }

                    }

                    try {
                        new updateTimeStatus(ruang).execute("");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    time += 5000;
                    h.postDelayed(this, 5000);
                }
            }, 5000); // 1 second delay (takes millis)

        }

    }

    private void getListRoom() {
        final internetData internet = new internetData(context);

        String listRoom;

        listRoom = internet.get_listRoom();
        try {
            sp = (Spinner) findViewById(R.id.pilih_ruang);

            JSONObject obj = new JSONObject(listRoom);
            JSONArray data = obj.getJSONArray("data");


            List<Integer> listId = new ArrayList<Integer>();
            List<String> listSpinner = new ArrayList<String>();
            for (int i = 0; i < data.length(); i++){
                JSONObject item = data.getJSONObject(i);
                listSpinner.add(item.getString("nama_ruang"));
                listId.add(item.getInt("id_ruang"));
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listSpinner);
            sp.setAdapter(adapter);

            sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    idRoom = listId.get(position);
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    //TODO : NEW FEATURE START HERE >>>

    private void setFNBClicked() {
        activityMainBinding.btnFnb.setOnClickListener(v -> initMainFnb());
        activityMainBinding.btnHelp.setOnClickListener(v -> initHelp());
    }

    private void attachFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container_main, fragment).addToBackStack(null).commit();
    }

    private void attachFragmentWithNoStack(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container_main, fragment).commit();
    }

    public void popBackStackFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
    }

    public void backFromFNB() {
        resetForm();
        findViewById(R.id.form_in_session).setVisibility(View.VISIBLE);
    }

    public void initMainFnb() {
        resetForm();
        frameLayout.setVisibility(View.VISIBLE);
        MainFnbFragment mainFnbFragment = new MainFnbFragment();
        attachFragment(mainFnbFragment);
    }

    public void initFnb(Bundle bundle) {
        FnbFragment fnbFragment = new FnbFragment();
        fnbFragment.setArguments(bundle);
        attachFragment(fnbFragment);
    }

    public void initOrder(Bundle bundle) {
        OrderFragment orderFragment = new OrderFragment();
        orderFragment.setArguments(bundle);
        attachFragmentWithNoStack(orderFragment);
    }

    public void initCart() {
        CartFragment cartFragment = new CartFragment();
        attachFragment(cartFragment);
    }

    public void initDetailMenu(Bundle bundle) {
        MenuFragment menuFragment = new MenuFragment();
        menuFragment.setArguments(bundle);
        attachFragment(menuFragment);
    }


    private void initHelp() {
        resetForm();
        frameLayout.setVisibility(View.VISIBLE);
        HelpFragment helpFragment = new HelpFragment();
        attachFragment(helpFragment);
    }

    public String time_diff(String a, String b) {

        String time1 = a + ":00";
        String time2 = b + ":00";
        String ret = "00:00:00";

        try {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
            Date date1 = format.parse(time1);
            Date date2 = format.parse(time2);

            if (date1.getTime() <= date2.getTime()) {

                long difference = date2.getTime() - date1.getTime();
                ret = secToTime(difference / 1000);

            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return ret;

    }

    public String secToTime(long sec) {
        long longVal = (long) sec;
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        String hour = Integer.toString(hours).length() == 1 ? "0" + Integer.toString(hours) : Integer.toString(hours);
        String min = Integer.toString(mins).length() == 1 ? "0" + Integer.toString(mins) : Integer.toString(mins);
        String second = Integer.toString(secs).length() == 1 ? "0" + Integer.toString(secs) : Integer.toString(secs);

        String res = hour + ":" + min + ":" + second;

        return res;

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (check_availability) {

            resetForm();

            if (session.getString("id_ruang", "").equals("")) {

                resetForm();
                showForm(R.id.form_setting);

            } else {

                if (!is_login()) {
                    resetForm();
                    showForm(R.id.form_login);
                } else {
                    new tabletStatus(ruang).execute("");
                }

            }

        }

    }

    public boolean is_login() {
        return is_login.equals("Y");
    }

    public void saveSetting(View v) {
        SharedPreferences.Editor setting = getSharedPreferences("session", MODE_PRIVATE).edit();
        setting.putString("id_ruang", Integer.toString(idRoom));
        setting.commit();
        resetForm();

        ruang = session.getString("id_ruang", "");

        new tabletStatus(ruang).execute("");

    }

    public void showForm(int v) {
        LinearLayout l1 = (LinearLayout) findViewById(v);
        l1.setVisibility(View.VISIBLE);
    }

    public void resetForm() {

        this.check_availability = true;

        goFullscreen();

        LinearLayout l1 = (LinearLayout) findViewById(R.id.form_login);
        l1.setVisibility(View.GONE);

        LinearLayout l2 = (LinearLayout) findViewById(R.id.main_form);
        l2.setVisibility(View.GONE);

        LinearLayout l3 = (LinearLayout) findViewById(R.id.form_start_session);
        l3.setVisibility(View.GONE);

        LinearLayout l4 = (LinearLayout) findViewById(R.id.form_in_session);
        l4.setVisibility(View.GONE);

        frameLayout.setVisibility(View.GONE);

        activityMainBinding.containerCart.setVisibility(View.GONE);

        LinearLayout l5 = (LinearLayout) findViewById(R.id.form_setting);
        l5.setVisibility(View.GONE);

        LinearLayout l6 = (LinearLayout) findViewById(R.id.form_end_session);
        l6.setVisibility(View.GONE);

        LinearLayout l7 = (LinearLayout) findViewById(R.id.form_schedule);
        l7.setVisibility(View.GONE);

    }

    public void goFullscreen() {

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        EditText pin = (EditText) findViewById(R.id.pin);
        EditText pin2 = (EditText) findViewById(R.id.text_pin);
        EditText id_reservation = (EditText) findViewById(R.id.id_reservation);
        EditText id_reservation2 = (EditText) findViewById(R.id.id_reservation2);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(pin.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(pin2.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(id_reservation.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(id_reservation2.getWindowToken(), 0);

    }

    public void login(View view) {
        goFullscreen();
        EditText pin = (EditText) findViewById(R.id.pin);
        new loginTask(pin.getText().toString()).execute("");
        pin.setText("");
    }

    public void back_home(View view) {
        resetForm();
        showForm(R.id.main_form);
    }

    public void back_session(View view) {
        goFullscreen();
        new tabletStatus(ruang).execute("");
    }

    public void startSession(View view) {
        resetForm();
        goFullscreen();
        showForm(R.id.form_start_session);
    }

    public void endSession(View view) {
        resetForm();
        goFullscreen();
        showForm(R.id.form_end_session);
    }

    public void scanForSession(View v) {

        Intent intent = new Intent(this, BarcodeCaptureActivity.class);
        intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
        intent.putExtra(BarcodeCaptureActivity.UseFlash, false);

        startActivityForResult(intent, RC_BARCODE_CAPTURE);
    }

    public void scanEndSession(View v) {

        Intent intent = new Intent(this, BarcodeCaptureActivity.class);
        intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
        intent.putExtra(BarcodeCaptureActivity.UseFlash, false);

        startActivityForResult(intent, RC_BARCODE_CAPTURE_END);
    }

    public void checkReservationCode(View v) {

        goFullscreen();
        EditText kode = (EditText) findViewById(R.id.id_reservation);
        if (kode.getText().length() < 6)
            Toast.makeText(context, "Please enter your valid Reservation Code !", Toast.LENGTH_SHORT).show();
        else
            new checkReservation(ruang, kode.getText().toString()).execute("");
    }

    public void endReservation(View v) {

        goFullscreen();
        final EditText kode = (EditText) findViewById(R.id.id_reservation2);
        if (kode.getText().length() < 6)
            Toast.makeText(context, "Please enter your valid Reservation Code !", Toast.LENGTH_SHORT).show();
        else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("End Session")
                    .setMessage("End your meeting session ?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new endReservation(ruang, kode.getText().toString()).execute("");
                            Button btn = (Button) findViewById(R.id.end_session_button);
                            btn.setText("ENDING SESSION...");
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }

    }

    public void lockTablet(View v) {
        goFullscreen();
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Lock tablet")
                .setMessage("Lock tablet ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor setting = getSharedPreferences("session", MODE_PRIVATE).edit();

                        setting.putString("is_login", "");

                        setting.commit();

                        is_login = "N";

                        new tabletStatus(ruang).execute("");

                    }

                })
                .setNegativeButton("No", null)
                .show();

    }

    public void goToSetting(View v) {
        resetForm();
        showForm(R.id.form_setting);

        LinearLayout l1 = (LinearLayout) findViewById(R.id.enter_pin);
        l1.setVisibility(View.VISIBLE);

        LinearLayout l2 = (LinearLayout) findViewById(R.id.setting_main);
        l2.setVisibility(View.GONE);
    }

    public void setting_pin(View v) {

        // Setting Page
        EditText pin = (EditText) findViewById(R.id.text_pin);
        goFullscreen();

        if (internetData.md5("vzt" + pin.getText().toString()).equals(session.getString("auth_pin", ""))) {

            LinearLayout l1 = (LinearLayout) findViewById(R.id.enter_pin);
            l1.setVisibility(View.GONE);

            LinearLayout l2 = (LinearLayout) findViewById(R.id.setting_main);
            l2.setVisibility(View.VISIBLE);

            pin.setText("");

        } else {

            Toast.makeText(context, "Incorrect PIN !", Toast.LENGTH_SHORT).show();

        }
    }

    public void viewSchedule(View v) {

        resetForm();
        showForm(R.id.form_schedule);

        new listSchedule(ruang).execute("");

    }

    // barcode scan
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        this.check_availability = false;

        if (requestCode == RC_BARCODE_CAPTURE) {

            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    new checkReservation(ruang, barcode.displayValue).execute("");
                } else {
                    //statusMessage.setText(R.string.barcode_failure);
                    Log.d("VIZITA", "No QR Code captured");
                }
            } else {
                Toast.makeText(context, "No QR Code Detected", Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == RC_BARCODE_CAPTURE_END) {

            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    new endReservation(ruang, barcode.displayValue).execute("");
                } else {
                    //statusMessage.setText(R.string.barcode_failure);
                    Log.d("VIZITA", "No QR Code captured");
                }
            } else {
                Toast.makeText(context, "No QR Code Detected", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    // The definition of our task class
    private class tabletStatus extends AsyncTask<String, Integer, String> {

        public String result, id, ruang, default_text;
        public Button btn;
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.VISIBLE);
        }

        public tabletStatus(String ruang) {
            this.ruang = ruang;
        }

        @Override
        protected String doInBackground(String... params) {

            final internetData internet = new internetData(context);

            this.result = internet.tablet_status(ruang);

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.GONE);

            if (this.result.equals("error")) {

                Toast.makeText(context, "Sorry, Internal Problem", Toast.LENGTH_SHORT).show();

                goFullscreen();

            } else {

                SharedPreferences.Editor setting = getSharedPreferences("session", MODE_PRIVATE).edit();

                try {

                    // Toast.makeText(context, "On this stage, view must be updated to current session.", Toast.LENGTH_SHORT).show();

                    JSONObject obj = new JSONObject(this.result);
                    JSONObject data = obj.getJSONObject("data");

                    setting.putString("id_ruang", data.getString("id_ruang"));
                    setting.putString("nama_ruang", data.getString("nama_ruang"));
                    setting.putString("status_ruang", data.getString("status_ruang"));
                    setting.putString("auth_pin", data.getString("auth_pin"));
                    setting.putString("kode_reservasi", data.getString("kode_reservasi"));
                    setting.putString("judul_reservasi", data.getString("judul_reservasi"));

                    DataSession dataSession = DataSession.INSTANCE;
                    dataSession.setKeyIdReservasi(data.getString("id_reservasi"));

                    setting.putString("nama_lengkap", data.getString("nama_lengkap"));
                    setting.putString("perusahaan", data.getString("perusahaan"));
                    setting.putString("est_time", data.getString("selesai_jam"));

                    setting.putString("is_active", data.getString("is_active"));
                    setting.putString("is_login", "Y");

                    TextView current_room = (TextView) findViewById(R.id.meeting_room_name);
                    current_room.setText(data.getString("nama_ruang"));

                    current_room = (TextView) findViewById(R.id.meeting_room_name2);
                    current_room.setText(data.getString("nama_ruang"));

                    current_room = (TextView) findViewById(R.id.meeting_room_name3);
                    current_room.setText(data.getString("nama_ruang"));

                    current_room = (TextView) findViewById(R.id.meeting_room_name4);
                    current_room.setText(data.getString("nama_ruang") + " Schedule");

                    setting.commit();

                    // Toast.makeText(context, "Welcome, " + data.getString("status_ruang") + " is ready !", Toast.LENGTH_SHORT).show();
                    resetForm();

                    if (!is_login()) {

                        showForm(R.id.form_login);

                    } else {

                        if (data.getString("status_ruang").equals("unavailable")) {

                            showForm(R.id.form_in_session);

                            TextView id = (TextView) findViewById(R.id.id_reservation2);
                            id.setText(data.getString("kode_reservasi"));

                            TextView nama = (TextView) findViewById(R.id.nama_lengkap);
                            nama.setText(data.getString("nama_lengkap"));

                            TextView judul = (TextView) findViewById(R.id.judul_rapat);
                            judul.setText(data.getString("judul_reservasi"));

                            TextView perusahaan = (TextView) findViewById(R.id.nama_perusahaan);
                            perusahaan.setText(data.getString("perusahaan"));

                            TextView est_time = (TextView) findViewById(R.id.est_time);
                            est_time.setText("Est. time finish : " + data.getString("selesai_jam"));

                        } else {
                            showForm(R.id.main_form);
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    // The definition of our task class
    private class updateTimeStatus extends AsyncTask<String, Integer, String> {

        public String result, id, ruang, result2, default_text;
        public Button btn;
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.GONE);
        }

        public updateTimeStatus(String ruang) {
            this.ruang = ruang;
        }

        @Override
        protected String doInBackground(String... params) {

            final internetData internet = new internetData(context);

            this.result = internet.tablet_status(ruang);

            this.result2 = internet.list_schedule(ruang);

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.GONE);

            if (this.result.equals("error")) {

                Toast.makeText(context, "Sorry, Internal Problem", Toast.LENGTH_SHORT).show();

                goFullscreen();

            } else {

                SharedPreferences.Editor setting = getSharedPreferences("session", MODE_PRIVATE).edit();

                try {

                    JSONObject obj = new JSONObject(this.result);
                    JSONObject data = obj.getJSONObject("data");

                    setting.putString("id_ruang", data.getString("id_ruang"));
                    setting.putString("nama_ruang", data.getString("nama_ruang"));
                    setting.putString("status_ruang", data.getString("status_ruang"));
                    setting.putString("auth_pin", data.getString("auth_pin"));
                    setting.putString("kode_reservasi", data.getString("kode_reservasi"));

                    setting.putString("nama_lengkap", data.getString("nama_lengkap"));
                    setting.putString("perusahaan", data.getString("perusahaan"));
                    setting.putString("est_time", data.getString("selesai_jam"));

                    setting.putString("is_active", data.getString("is_active"));
                    setting.putString("is_login", "Y");

                    TextView est_time = (TextView) findViewById(R.id.est_time);
                    est_time.setText("Est. time finish : " + data.getString("selesai_jam"));

                    setting.commit();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            if (this.result2.equals("failed")) {

                Toast.makeText(context, "Error gathering schedule", Toast.LENGTH_SHORT).show();
                goFullscreen();

            } else {

                try {

                    TextView ld = (TextView) findViewById(R.id.ld);

                    LinearLayout lv = (LinearLayout) findViewById(R.id.list_schedule);
                    lv.removeAllViews();

                    TextView txt_no_schedule = new TextView(context);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    txt_no_schedule.setLayoutParams(lp);
                    txt_no_schedule.setTextColor(Color.parseColor("#FFFFFF"));

                    JSONObject obj = new JSONObject(this.result2);
                    JSONArray data = obj.getJSONArray("data");

                    if (data.length() == 0) {

                        lv.addView(txt_no_schedule);
                        txt_no_schedule.setText("No schedule for this room.");

                    } else {

                        for (int i = 0; i < data.length(); i++) {

                            JSONObject item = data.getJSONObject(i);

                            View v = getLayoutInflater().inflate(R.layout.list_schedule, null);

                            TextView nama = (TextView) v.findViewById(R.id.name);
                            TextView perusahaan = (TextView) v.findViewById(R.id.company);
                            TextView time = (TextView) v.findViewById(R.id.time);

                            nama.setText(item.getString("nama_lengkap"));
                            perusahaan.setText(item.getString("perusahaan"));
                            time.setText(item.getString("time"));

                            lv.addView(v);

                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    // The definition of our task class
    private class loginTask extends AsyncTask<String, Integer, String> {

        public String result, id, pin;
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.VISIBLE);
        }

        public loginTask(String pin) {
            this.pin = pin;
        }

        @Override
        protected String doInBackground(String... params) {

            final internetData internet = new internetData(context);

            String ruang = session.getString("id_ruang", "");

            this.result = internet.login(ruang, pin);

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.GONE);

            if (this.result.equals("error")) {
                Toast.makeText(context, "Incorrect PIN !", Toast.LENGTH_SHORT).show();
                goFullscreen();

            } else {

                is_login = "Y";
                new tabletStatus(ruang).execute("");

            }

        }
    }

    // The definition of our task class
    private class checkReservation extends AsyncTask<String, Integer, String> {

        public String result, id, ruang, kode_reservasi;
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.VISIBLE);

        }

        public checkReservation(String ruang, String kode_reservasi) {
            this.ruang = ruang;
            this.kode_reservasi = kode_reservasi;
        }

        @Override
        protected String doInBackground(String... params) {

            final internetData internet = new internetData(context);

            this.result = internet.check_reservation(ruang, this.kode_reservasi);

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.GONE);

//            resetForm();
//
//            showForm(R.id.form_in_session);

            //TODO: DISABLE SEMENTARA
            if (this.result.equals("not_found")) {

                Toast.makeText(context, "Reservation ID is Invalid !", Toast.LENGTH_SHORT).show();
                goFullscreen();

            } else {

                SharedPreferences.Editor setting = getSharedPreferences("session", MODE_PRIVATE).edit();

                Log.e("WOKEEE", "BERHASIL !!!");

                try {

                    JSONObject obj = new JSONObject(this.result);
                    JSONObject data = obj.getJSONObject("data");

                    setting.putString("kode_reservasi", data.getString("kode_reservasi"));
                    setting.putString("nama_lengkap", data.getString("nama_lengkap"));
                    setting.putString("perusahaan", data.getString("perusahaan"));
                    setting.putString("est_time", data.getString("selesai_jam"));
                    setting.putString("judul_reservasi", data.getString("judul_reservasi"));

                    DataSession dataSession = DataSession.INSTANCE;
                    dataSession.setKeyIdReservasi(data.getString("id_reservasi"));

                    TextView judul = (TextView) findViewById(R.id.judul_rapat);
                    judul.setText(data.getString("judul_reservasi"));

                    setting.commit();

                    resetForm();

                    showForm(R.id.form_in_session);

                    TextView id_reservation = (TextView) findViewById(R.id.id_reservation2);
                    id_reservation.setText(data.getString("kode_reservasi"));

                    TextView nama = (TextView) findViewById(R.id.nama_lengkap);
                    nama.setText(data.getString("nama_lengkap"));

                    TextView perusahaan = (TextView) findViewById(R.id.nama_perusahaan);
                    perusahaan.setText(data.getString("perusahaan"));

                    TextView est_time = (TextView) findViewById(R.id.est_time);
                    est_time.setText("Est. time finish " + data.getString("selesai_jam"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    // The definition of our task class
    private class endReservation extends AsyncTask<String, Integer, String> {

        public String result, id, ruang, kode_reservasi;
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.GONE);

        }

        public endReservation(String ruang, String kode_reservasi) {
            this.ruang = ruang;
            this.kode_reservasi = kode_reservasi;
        }

        @Override
        protected String doInBackground(String... params) {

            final internetData internet = new internetData(context);

            this.result = internet.end_reservation(ruang, this.kode_reservasi);

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.GONE);

            if (this.result.equals("not_found")) {
                Toast.makeText(context, "Reservation ID is Invalid !", Toast.LENGTH_SHORT).show();
                goFullscreen();

            } else {

                SharedPreferences.Editor setting = getSharedPreferences("session", MODE_PRIVATE).edit();

                try {

                    JSONObject obj = new JSONObject(this.result);
                    JSONObject data = obj.getJSONObject("data");

                    setting.putString("status_ruang", "available");

                    setting.putString("nama_lengkap", "");
                    setting.putString("perusahaan", "");
                    setting.putString("est_time", "");

                    setting.commit();

                    new tabletStatus(ruang).execute("");

                    Button btn = (Button) findViewById(R.id.end_session_button);
                    btn.setText("END SESSION");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    // The definition of our task class
    private class listSchedule extends AsyncTask<String, Integer, String> {

        public String result, id, ruang, kode_reservasi;
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.VISIBLE);

        }

        public listSchedule(String ruang) {
            this.ruang = ruang;
            this.kode_reservasi = kode_reservasi;
        }

        @Override
        protected String doInBackground(String... params) {

            final internetData internet = new internetData(context);

            this.result = internet.list_schedule(ruang);

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            TextView loading = (TextView) findViewById(R.id.loading);
            loading.setVisibility(View.GONE);

            if (this.result.equals("failed")) {

                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                goFullscreen();

            } else {

                try {

                    TextView ld = (TextView) findViewById(R.id.ld);

                    LinearLayout lv = (LinearLayout) findViewById(R.id.list_schedule);
                    lv.removeAllViews();

                    TextView txt_no_schedule = new TextView(context);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    txt_no_schedule.setLayoutParams(lp);
                    txt_no_schedule.setTextColor(Color.parseColor("#FFFFFF"));

                    JSONObject obj = new JSONObject(this.result);
                    JSONArray data = obj.getJSONArray("data");

                    if (data.length() == 0) {


                        lv.addView(txt_no_schedule);
                        txt_no_schedule.setText("No schedule for this room.");

                    } else {

                        for (int i = 0; i < data.length(); i++) {

                            JSONObject item = data.getJSONObject(i);

                            View v = getLayoutInflater().inflate(R.layout.list_schedule, null);

                            TextView nama = (TextView) v.findViewById(R.id.name);
                            TextView perusahaan = (TextView) v.findViewById(R.id.company);
                            TextView time = (TextView) v.findViewById(R.id.time);

                            nama.setText(item.getString("nama_lengkap"));
                            perusahaan.setText(item.getString("perusahaan"));
                            time.setText(item.getString("time"));

                            lv.addView(v);

                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    }

}
