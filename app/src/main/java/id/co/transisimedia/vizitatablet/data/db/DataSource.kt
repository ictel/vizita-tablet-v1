package id.co.transisimedia.vizitatablet.data.db

import id.co.transisimedia.vizitatablet.App
import id.co.transisimedia.vizitatablet.data.models.Menu
import id.co.transisimedia.vizitatablet.utils.doBack
import id.co.transisimedia.vizitatablet.utils.loge
import id.co.transisimedia.vizitatablet.utils.logi
import io.reactivex.Observable

/**
 * Created by fizhu on 13,June,2020
 * Hvyz.anbiya@gmail.com
 */
open class DataSource {
    private val cartDao: CartDao
    open val listMenu: Observable<List<Menu>>
    open val listCart: Observable<List<Menu>>

    init {
        val db = Db.getInstance(App.getInstance!!)
        cartDao = db.cartdao()
        listMenu = cartDao.all
        listCart = cartDao.getListCart
    }

    fun insertAll(list: List<Menu>) {
        doBack(
                action = {
                    cartDao.insertAll(list)
                },
                success = {
                    logi("size masuk db = "+list.size)
                    logi("success insert all menu to cart") },
                error = { loge("failed insert all menu to cart") }
        )
    }

    fun insert(menu: Menu) {
        doBack(
                action = {
                    cartDao.insert(menu)
                },
                success = { logi("success insert menu to cart") },
                error = { loge("failed insert menu to cart") }
        )
    }

    fun updateNote(id: String, note: String) {
        doBack(
                action = {
                    cartDao.updateNotes(id, note)
                },
                success = { logi("success update note menu in cart") },
                error = { loge("failed update note menu in cart") }
        )
    }

    fun updateQuantity(id: String, quantity: Int) {
        doBack(
                action = {
                    cartDao.updateQuantity(id, quantity)
                },
                success = { logi("success update item quantity") },
                error = { loge("failed update item quantity") }
        )
    }

    fun deleteAll() {
        doBack(
                action = {
                    cartDao.deleteAll()
                },
                success = { logi("success delete menu from cart") },
                error = { loge("failed delete menu from cart") }
        )
    }

    fun delete(id: String) {
        doBack(
                action = {
                    cartDao.delete(id)
                },
                success = { logi("success delete all menu from cart") },
                error = { loge("failed delete all menu from cart") }
        )
    }
}