package id.co.transisimedia.vizitatablet.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import id.co.transisimedia.vizitatablet.MainActivity
import id.co.transisimedia.vizitatablet.databinding.DialogAuthBinding

/**
 * Created by fizhu on 24,May,2020
 * Hvyz.anbiya@gmail.com
 */
class AuthDialog(
        context: Context,
        private val onButtonClicked: (dialog: AuthDialog, code: String) -> Unit,
        private val onQRClicked: (dialog: AuthDialog) -> Unit,
        private val onBackClicked: (dialog: AuthDialog) -> Unit
) : Dialog(context) {

    private lateinit var binding: DialogAuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
        window!!.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setLayout(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT
            )
        }
        onInit()
    }

    private fun onInit() {
        initView()
    }

    private fun initView() {
        binding.btnBackFnb.setOnClickListener {
            onBackClicked.invoke(this)
            this.dismiss()
        }

        binding.btnSubmitAuth.setOnClickListener {
            val code = binding.etAuth.text.toString().trim()
            if (code.isNotBlank() || code != "" || code.isNotEmpty()) {
                onButtonClicked.invoke(this,code ?: "")
                this.dismiss()
            } else {
                Toast.makeText(context, "Code tidak boleh kosong !", Toast.LENGTH_SHORT).show()
            }
        }

        binding.btnScanAuth.setOnClickListener {
            onQRClicked.invoke(this)
            this.dismiss()
        }
    }

    override fun onBackPressed() {
        //do nothing
    }

}