package id.co.transisimedia.vizitatablet.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import id.co.transisimedia.vizitatablet.MainActivity
import id.co.transisimedia.vizitatablet.databinding.FragmentHelpBinding

/**
 * Created by fizhu on 12,June,2020
 * Hvyz.anbiya@gmail.com
 */
class HelpFragment : Fragment() {

    private var binding: FragmentHelpBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHelpBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInit()
    }

    private fun onInit() {
        binding?.btnClose?.setOnClickListener { (activity as MainActivity).backFromFNB() }
        handleBackpressed()
    }

    private fun handleBackpressed() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity).backFromFNB()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }
}