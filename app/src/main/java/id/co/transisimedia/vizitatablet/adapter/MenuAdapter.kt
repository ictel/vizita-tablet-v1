package id.co.transisimedia.vizitatablet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.co.transisimedia.vizitatablet.R
import id.co.transisimedia.vizitatablet.data.models.Menu
import id.co.transisimedia.vizitatablet.databinding.ItemListMenuBinding

/**
 * Created by fizhu on 12,June,2020
 * Hvyz.anbiya@gmail.com
 */

class MenuAdapter(
        private val context: Context,
        private val onPlusClicked: (id: String, quantity: Int) -> Unit,
        private val onMinusClicked: (id: String, quantity: Int) -> Unit
) : RecyclerView.Adapter<MenuAdapter.ViewHolder>() {

    private val list: MutableList<Menu> = mutableListOf()

    fun setData(listMenu: List<Menu>) {
        list.clear()
        list.addAll(listMenu)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemListMenuBinding.inflate(LayoutInflater.from(context), parent, false).root)
    }

    override fun getItemCount(): Int = list.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemListMenuBinding.bind(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]
        with(holder.binding) {
            tvName.text = data.name
            tvDesc.text = data.description

            val price = data.price as String

            if (price == "0"){
                tvPrice.text = "Free"
            }else{
                tvPrice.text = "Rp. ${data.price}"
            }

            Glide.with(context)
                    .load(data.file)
                    .placeholder(R.drawable.icon)
                    .error(R.drawable.icon)
                    .into(ivMenu)

            var quantity: Int = data.quantity ?: 0
            val stock: Int = data.stock ?: 0

            tvQuantity.text = quantity.toString()

            btnPlus.setOnClickListener {
                if (quantity < stock) {
                    quantity += 1
                    onPlusClicked.invoke(data.id, quantity)
                    setVisibility(tvQuantity, btnMinus, quantity)
                }
            }
            btnMinus.setOnClickListener {
                if (quantity > 0) {
                    quantity -= 1
                    onMinusClicked.invoke(data.id, quantity)
                    setVisibility(tvQuantity, btnMinus, quantity)
                }
            }

            if (stock == 0) {
                tvName.setTextColor(ContextCompat.getColor(context, R.color.text_disabled))
                tvDesc.setTextColor(ContextCompat.getColor(context, R.color.text_disabled))
                tvPrice.setTextColor(ContextCompat.getColor(context, R.color.text_disabled))
                btnPlus.isEnabled = false
                btnPlus.setBackgroundResource(R.drawable.rounded_grey)
            }

            setVisibility(tvQuantity, btnMinus, quantity)
        }
    }

    private fun setVisibility(tvQty: TextView, btnMinus: ImageView, quantity: Int) {
        if (quantity == 0) {
            tvQty.visibility = View.GONE
            btnMinus.visibility = View.GONE
        } else {
            tvQty.visibility = View.VISIBLE
            btnMinus.visibility = View.VISIBLE
        }
    }
}