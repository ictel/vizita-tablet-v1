package id.co.transisimedia.vizitatablet.libs;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

public class internetData {

    private Context context;

    public String url = "https://vizita.id/vizita/api";

    public internetData(Context context){
        this.context = context;
    }

    public String getInternetData(String url) throws Exception {

        Log.d("IT Meth", "Method Called");

        BufferedReader in = null;
        String data = null;

        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient client = new DefaultHttpClient();
            URI web = new URI(url.replace(" ", "+").replace("^", ""));
            // Log.d("ITet URL", url.replace(" ", "+").replace("^", ""));
            HttpGet request = new HttpGet();
            request.setURI(web);
            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            // String nl = System.getProperty("line.separator");
            String nl = "";
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
        } finally {
            if (in != null) {
                try {
                    in.close();
                    return data;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return data;

    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private boolean ifConnect(){

        ConnectivityManager conn = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network = conn.getActiveNetworkInfo();

        return network != null && network.isConnected();

    }

    public String login(String ruang, String pin){

        String response = "";

        pin = md5("vzt" + pin);

        try {
            response = this.getInternetData(url + "/tablet/auth/" + ruang + "/" + pin);
            // Log.d("ITet Res ", response);
        }catch (Exception e){
            Log.d("Error ", e.toString());
        }

        return response;

    }

    public String tablet_status(String ruang){

        String response = "";

        try {
            response = this.getInternetData(url + "/tablet/status/" + ruang);
            // Log.d("ITet Res ", response);
        }catch (Exception e){
            Log.d("Error ", e.toString());
        }

        return response;

    }

    public String list_schedule(String ruang){

        String response = "";

        try {
            response = this.getInternetData(url + "/tablet/schedule/" + ruang);
            // Log.d("ITet Res ", response);
        }catch (Exception e){
            Log.d("Error ", e.toString());
        }

        return response;

    }

    public String check_reservation(String ruang, String kode_reservasi){

        String response = "";

        try {
            response = this.getInternetData(url + "/tablet/reservation/" + ruang + "/" + kode_reservasi);
//             Log.d("ITet Res ", response);
        }catch (Exception e){
            Log.d("Error ", e.toString());
        }

        return response;

    }

    public String get_listRoom(){

        String response = "";

        try {
            response = this.getInternetData(url + "/tablet/listRoom");
        }catch (Exception e){
            Log.d("Error ", e.toString());
        }

        return response;

    }

    public String end_reservation(String ruang, String kode_reservasi){

        String response = "";

        try {
            response = this.getInternetData(url + "/tablet/end_reservation/" + ruang + "/" + kode_reservasi);
            // Log.d("ITet Res ", response);
        }catch (Exception e){
            Log.d("Error ", e.toString());
        }

        return response;

    }

    public static Drawable loadImage(String url) {

        try {
            url = url.replace(" ", "%20");
	        /* Strict Mode ! */
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            Log.d("ITet load", "loaded");
            return d;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("ITet load", "Could not load");
            return null;
        }
    }

    // input usulan

    public String sendPostRequest(String action, HashMap<String, String> postDataParams) {

        String response = "";

        try {

            URL link = new URL(url + "?action=" + action);

            HttpURLConnection conn = (HttpURLConnection) link.openConnection();
            conn.setReadTimeout(30000);
            conn.setConnectTimeout(30000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();
            String line;

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            } else {
                response = "Error Registering";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public String get_produk(String s) {

        String response = "";

        try {
            response = this.getInternetData(url + "?action=get_products&category=" + s);
            // // Log.d("ITet Res ", response);
        }catch (Exception e){
            // Log.d("Error ", e.toString());
            response = "Failed to connect";
        }

        return response;

    }

    public String get_keranjang(String s, String id_transaksi) {

        String response = "";

        try {
            response = this.getInternetData(url + "?action=get_keranjang&id_user=" + s + "&id_transaksi=" + id_transaksi);
            // // Log.d("ITet Res ", response);
        }catch (Exception e){
            // Log.d("Error ", e.toString());
            response = "Failed to connect";
        }

        return response;

    }

    public String hapus_keranjang(String id_user, String id_produk) {

        String response = "";

        try {
            response = this.getInternetData(url + "?action=hapus_keranjang&id_user=" + id_user + "&id_produk=" + id_produk);
            // // Log.d("ITet Res ", response);
        }catch (Exception e){
            // Log.d("Error ", e.toString());
            response = "Failed to connect";
        }

        return response;

    }

    public String add_to_cart(String id_user, String id_produk, String qty) {

        String response = "";

        try {
            response = this.getInternetData(url + "?action=add_to_cart&id_user=" + id_user + "&id_produk=" + id_produk + "&qty=" + qty);
            // // Log.d("ITet Res ", response);
        }catch (Exception e){
            // Log.d("Error ", e.toString());
            response = "Failed to connect";
        }

        return response;

    }

    public String konfirmasi(String id_user, String id_transaksi, String bank, String atas_nama, String nominal, String tanggal) {

        String response = "";

        try {
            response = this.getInternetData(url + "?action=konfirmasi_pembayaran&id_user=" + id_user
                    + "&id_transaksi=" + id_transaksi
                    + "&bank=" + bank
                    + "&atas_nama=" + atas_nama
                    + "&nominal=" + nominal
                    + "&tanggal=" + tanggal);
            // // Log.d("ITet Res ", response);
        }catch (Exception e){
            // Log.d("Error ", e.toString());
            response = "Failed to connect";
        }

        return response;

    }

    public String get_history(String id_user) {

        String response = "";

        try {
            response = this.getInternetData(url + "?action=get_transaction_history&id_user=" + id_user);
            // // Log.d("ITet Res ", response);
        }catch (Exception e){
            // Log.d("Error ", e.toString());
            response = "Failed to connect";
        }

        return response;

    }

    public String get_profile(String id_user) {

        String response = "";

        try {
            response = this.getInternetData(url + "?action=get_profile&id_user=" + id_user);
            // // Log.d("ITet Res ", response);
        }catch (Exception e){
            // Log.d("Error ", e.toString());
            response = "Failed to connect";
        }

        return response;

    }

    public String checkout(String id_user, String atas_nama, String alamat_pengiriman, String no_telp, String tipe_pembayaran,
                           String kurir, String total_harga, String total_qty) {

        String response = "";

        try {
            response = this.getInternetData(url + "?action=checkout&id_user=" + id_user + "&atas_nama=" + atas_nama
                    + "&alamat_pengiriman=" + alamat_pengiriman + "&tipe_pembayaran=" + tipe_pembayaran + "&kurir=" + kurir
                    + "&total_harga=" + total_harga + "&total_qty=" + total_qty + "&no_telp=" + no_telp);
            // // Log.d("ITet Res ", response);
        }catch (Exception e){
            // Log.d("Error ", e.toString());
            response = "Failed to connect";
        }

        return response;

    }

    public String register(String nama, String alamat, String no_telp, String user, String email, String pass, String gender) {

        String response = "";

        try {
            response = this.getInternetData(url + "?action=register&nama=" + nama + "&username=" + user
                    + "&alamat=" + alamat + "&no_telp=" + no_telp + "&email=" + email + "&password=" + pass + "&gender=" + gender);
            // // Log.d("ITet Res ", response);
        }catch (Exception e){
            // Log.d("Error ", e.toString());
            response = "Failed to connect";
        }

        return response;

    }

}