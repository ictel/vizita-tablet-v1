package id.co.transisimedia.vizitatablet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.co.transisimedia.vizitatablet.App
import id.co.transisimedia.vizitatablet.App.Companion.context
import id.co.transisimedia.vizitatablet.R
import id.co.transisimedia.vizitatablet.data.models.Menu
import kotlinx.android.synthetic.main.item_list_menu.view.*

/**
 * Created by Yudi Kurniyawan on 25/06/2020
 */
class MenusAdapter (listener: OnItemClickListener) : RecyclerView.Adapter<MenusAdapter.MenuViewHolder>() {

    var mOnItemClickListener : OnItemClickListener = listener

    private var listMenu = ArrayList<Menu>()

    fun setMenu(menu: List<Menu>?) {
        if (menu == null) return
        listMenu.clear()
        listMenu.addAll(menu)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_menu, parent, false)
        return MenuViewHolder(
                view
        )
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        val menu = listMenu[position]
        holder.bind(menu, mOnItemClickListener, position)
    }

    override fun getItemCount(): Int = listMenu.size


    class MenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(menu: Menu, listener: OnItemClickListener, position: Int) {

            with(itemView) {

                tv_name.text = menu.name
                tv_price.text = "Rp. ${menu.price}"
                tv_desc.text = menu.description

                Glide.with(context)
                        .load(menu.file)
                        .placeholder(R.drawable.icon)
                        .error(R.drawable.icon)
                        .into(iv_menu)

                var qty : Int = menu.quantity ?: 0
                val stock : Int = menu.stock ?: 0

                if (stock == 0){
                    tv_name.setTextColor(ContextCompat.getColor(App.context as Context, R.color.text_disabled))
                    tv_desc.setTextColor(ContextCompat.getColor(App.context as Context, R.color.text_disabled))
                    tv_price.setTextColor(ContextCompat.getColor(App.context as Context, R.color.text_disabled))
                    btn_plus.isEnabled = false
                    btn_plus.setBackgroundResource(R.drawable.rounded_grey)
                    btn_minus.isEnabled = false
                    btn_minus.setBackgroundResource(R.drawable.rounded_grey)
                }

                tv_quantity.text = qty.toString()

                btn_plus.setOnClickListener {
                    if (qty < stock) {
                        qty += 1
                        listener.onClickItem(menu, qty, position)
                        setVisibility(tv_quantity, btn_minus, qty)
                    }
                }

                btn_minus.setOnClickListener {
                    if (qty > 0) {
                        qty -= 1
                        listener.onClickItem(menu, qty, position)
                        setVisibility(tv_quantity, btn_minus, qty)
                    }
                }

                setVisibility(tv_quantity, btn_minus, qty)

            }
        }

        private fun setVisibility(tvQty: TextView, btnMinus: ImageView ,quantity: Int) {
            if (quantity == 0) {
                tvQty.visibility = View.GONE
                btnMinus.visibility = View.GONE
            } else {
                tvQty.visibility = View.VISIBLE
                btnMinus.visibility = View.VISIBLE
            }
        }
    }

    interface OnItemClickListener {
        fun onClickItem(menu: Menu, qty : Int, position: Int)
    }

}