package id.co.transisimedia.vizitatablet.data.api

import com.google.gson.JsonObject
import id.co.transisimedia.vizitatablet.BuildConfig
import id.co.transisimedia.vizitatablet.data.models.*
import id.co.transisimedia.vizitatablet.utils.baseUrl
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

/**
 * Created by fizhu on 22,May,2020
 * Hvyz.anbiya@gmail.com
 */

@JvmSuppressWildcards
interface Api {

    @Headers("Content-Type: application/json")
    @POST(ApiEndPoint.ENDPOINT_CANCEL_ORDER)
    fun cancelOrder(
            @Body body: HashMap<String, String>
    ): Observable<ApiDataResponse<OrderData>>

    @Headers("Content-Type: application/json")
    @POST(ApiEndPoint.ENDPOINT_ORDER)
    fun order(
            @Body body: JsonObject
    ): Observable<ApiDataResponse<OrderData>>

    @Headers("Content-Type: application/json")
    @POST(ApiEndPoint.ENDPOINT_AUTH_MERCHANT_BY_RESERVATION_ID)
    fun authMerchantReservasi(
            @Body body: HashMap<String, String>
    ): Observable<ApiDataResponse<Karyawan>>

    @Headers("Content-Type: application/json")
    @POST(ApiEndPoint.ENDPOINT_AUTH_MERCHANT)
    fun authMerchant(
            @Body body: HashMap<String, String>
    ): Observable<ApiDataResponse<Karyawan>>

    @GET(ApiEndPoint.ENDPOINT_GET_LIST_MERCHANT)
    fun getListMerchant(
            @Query("page") page: String,
            @Query("type") type: String
    ): Observable<ApiDataResponse<ListWrapper<Merchant>>>

    @GET(ApiEndPoint.ENDPOINT_DETAIL_MERCHANT)
    fun getDetailMerchant(
            @Query("id_merchant") id_merchant: Int,
            @Query("page") page: String
    ): Observable<ApiDataResponse<DetailMerchant>>

    @GET(ApiEndPoint.ENDPOINT_GET_LIST_ROOM)
    fun getListRoom(): Observable<ApiListResponse<ListRoom>>

    @GET(ApiEndPoint.ENDPOINT_GET_DETAIL_ORDER)
    fun getDetailOrder(
            @Query("idOrder") idOrder: String
    ): Observable<ApiDataResponse<DetailOrderData>>

    companion object {
        private fun provideOkHttpClient(): OkHttpClient {
            val client = OkHttpClient.Builder()
                    .callTimeout(1, TimeUnit.MINUTES)
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .addInterceptor(
                            if (BuildConfig.DEBUG) {
                                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS)
                                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
                            } else {
                                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
                            }
                    )
            return client.build()
        }

        fun create(): Api {
            val builder = Retrofit.Builder()
                    .baseUrl(baseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(provideOkHttpClient())
                    .build()

            return builder.create(Api::class.java)
        }
    }

}