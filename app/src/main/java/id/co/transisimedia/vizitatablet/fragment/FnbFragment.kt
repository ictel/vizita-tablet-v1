package id.co.transisimedia.vizitatablet.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import id.co.transisimedia.vizitatablet.MainActivity
import id.co.transisimedia.vizitatablet.adapter.MerchantAdapter
import id.co.transisimedia.vizitatablet.data.api.Api
import id.co.transisimedia.vizitatablet.data.db.DataSource
import id.co.transisimedia.vizitatablet.data.models.Merchant
import id.co.transisimedia.vizitatablet.databinding.FragmentFnbBinding
import id.co.transisimedia.vizitatablet.utils.DataSession
import id.co.transisimedia.vizitatablet.utils.loge
import id.co.transisimedia.vizitatablet.utils.route
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_fnb.*


/**
 * Created by fizhu on 12,June,2020
 * Hvyz.anbiya@gmail.com
 */
class FnbFragment : Fragment() {

    private var binding: FragmentFnbBinding? = null
    private val compositeDisposable by lazy { CompositeDisposable() }
    private val dataSource by lazy { DataSource() }
    private var currentPage = 1
    private var totaldata = 0
    private var totalpage: Int = 0
    private var posSatu = 1
    private var posDua = 2
    private var posTiga = 3
    private var merchType: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentFnbBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInit()
    }

    private fun onInit() {
        merchType = arguments?.getInt("merchType") ?: 0
        DataSession.clear()
        handleBackpressed()
        getTotalData()
        getListMerchant(currentPage)
        binding?.btnClose?.setOnClickListener { (activity as MainActivity).popBackStackFragment() }
    }

    override fun onResume() {
        super.onResume()
        try {
            dataSource.deleteAll()
        } catch (e: Exception) {
            loge(e.localizedMessage)
        }
    }

    private fun initSumPage() {
        if (totaldata != 0) {
            if (totaldata % 4 == 0) {
                totalpage = (totaldata / 4)
            } else {
                totalpage = (totaldata / 4) + 1
            }
            initNumberPage()
            initPageClick()
        }
    }

    private fun initNumberPage() {
        btn_1st.text = posSatu.toString()
        btn_2nd.text = posDua.toString()
        btn_3rd.text = posTiga.toString()
        btn_5th.text = totalpage.toString()
    }

    private fun initPageClick() {

        btn_previous.setOnClickListener {
            if (currentPage > 1) {
                Log.d("ANJAYBRO", " $currentPage")
                if (posSatu > 1) {
                    if (currentPage == posSatu) {
                        posSatu -= 1
                        posDua -= 1
                        posTiga -= 1
                        initNumberPage()
                    }
                }
                currentPage -= 1
                getListMerchant(currentPage)
            } else if (currentPage == 1) {
                currentPage = 1
                getListMerchant(currentPage)
                Toast.makeText(activity, "First Page", Toast.LENGTH_SHORT).show()
            }
        }

        btn_next.setOnClickListener {
            if (currentPage < totalpage) {
                if (posTiga < totalpage) {
                    if (currentPage == posTiga) {
                        val lastpage = totalpage - currentPage
                        if (lastpage != 1) {
                            posSatu += 1
                            posDua += 1
                            posTiga += 1
                            initNumberPage()
                        }
                    }
                    currentPage += 1
                    getListMerchant(currentPage)
                }
            } else if (currentPage == totalpage) {
                currentPage = totalpage
                getListMerchant(totalpage)
                Toast.makeText(activity, "Last Page", Toast.LENGTH_SHORT).show()
            }
        }

        btn_4th.setOnClickListener {
            currentPage = posTiga
            val lastpage = totalpage - posTiga
            if (lastpage == 3) {
                posSatu += 2
                posDua += 2
                posTiga += 2
                initNumberPage()
            } else if (lastpage == 2) {
                posSatu += 1
                posDua += 1
                posTiga += 1
                initNumberPage()
            } else if (lastpage == 1) {
                initNumberPage()
            } else {
                posSatu += 3
                posDua += 3
                posTiga += 3
                initNumberPage()
            }
            getListMerchant(currentPage)
        }

        btn_5th.setOnClickListener {
            currentPage = totalpage
            getListMerchant(currentPage)
        }

        btn_1st.setOnClickListener {
            val getpos = btn_1st.text.toString()
            currentPage = getpos.toInt()
            getListMerchant(currentPage)
        }

        btn_2nd.setOnClickListener {
            val getpos = btn_2nd.text.toString()
            currentPage = getpos.toInt()
            getListMerchant(currentPage)
        }

        btn_3rd.setOnClickListener {
            val getpos = btn_3rd.text.toString()
            currentPage = getpos.toInt()
            getListMerchant(currentPage)
        }

    }

    private fun handleBackpressed() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity).popBackStackFragment()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private fun initRv(list: List<Merchant>) {
        val adapterMerchant =
                MerchantAdapter(list, requireContext()) {
                    if (it.operational?.status == "Open") {
                        val bundle = Bundle()
                        bundle.putInt("id", it.id ?: 0)
                        (activity as MainActivity).initDetailMenu(bundle)
                    } else {
                        showDialogClosed()
                    }
                }
        with(binding?.rvMerchant!!) {
            layoutManager = GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
            adapter = adapterMerchant
        }
        adapterMerchant.notifyDataSetChanged()
    }

    private fun showDialogClosed() {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setCancelable(false)
                setTitle("Oops !")
                setMessage("This merhant is currently closed right now")
                setPositiveButton("Okay"
                ) { dialog, _ ->
                    (activity as MainActivity).goFullscreen()
                    dialog.dismiss()
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    private fun getListMerchant(page: Int) {
        show(View.GONE, View.VISIBLE, View.GONE)
        compositeDisposable.route(
                Api.create().getListMerchant(page.toString(), merchType.toString()),
                main = {
                    if (it.status) {
                        if (it.data?.data != null) {
                            show(View.VISIBLE, View.GONE, View.GONE)
                            initRv(it.data.data)
                        } else {
                            show(View.GONE, View.GONE, View.VISIBLE)
                        }
                    } else {
                        show(View.GONE, View.GONE, View.VISIBLE)
                    }
                },
                error = {
                    show(View.GONE, View.GONE, View.VISIBLE)
                    loge(it.localizedMessage)
                }
        )
    }

    private fun getTotalData() {
        compositeDisposable.route(
                Api.create().getListMerchant(currentPage.toString(), merchType.toString()),
                main = {
                    if (it.status) {
                        if (it.data != null) {
                            totaldata = it.data.totalData
                            initSumPage()
                        } else {
                            Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                    }
                },
                error = {
                    Toast.makeText(context, it.localizedMessage, Toast.LENGTH_SHORT).show()
                    loge(it.localizedMessage)
                }
        )
    }

    private fun show(rv: Int, pb: Int, err: Int) {
        binding?.rvMerchant?.visibility = rv
        binding?.pb?.visibility = pb
        binding?.tvNodata?.visibility = err
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }

}