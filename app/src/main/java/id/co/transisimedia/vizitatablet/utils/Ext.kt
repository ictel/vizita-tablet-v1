package id.co.transisimedia.vizitatablet.utils

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.util.Log
import id.co.transisimedia.vizitatablet.BuildConfig

/**
 * Created by fizhu on 10,June,2020
 * Hvyz.anbiya@gmail.com
 */

/** The loading progress dialog object */
var progressDialog: ProgressDialog? = null

fun logi(pesan: String?) = Log.d("VIZITA-LOG D","Message : $pesan")

fun loge(pesan: String?) = Log.e("VIZITA-LOG E","Message : $pesan")

fun baseUrl(): String = BuildConfig.BASE_URL

/**
 * Shows a loading progress dialog.
 * @param context the context
 * @param message the dialog message string
 * @param onBackPressListener the back button press listener when loading is shown
 */
fun showProgressDialog(context: Context, message: String) {
    dismissProgressDialog()
    progressDialog = ProgressDialog(context)
    progressDialog!!.setMessage(message)
    progressDialog!!.setCancelable(false)
    if (context is Activity && !context.isFinishing) progressDialog!!.show()
}

/** Hides the currently shown loading progress dialog */
fun dismissProgressDialog() {
    if (progressDialog != null && progressDialog!!.isShowing) {
        progressDialog!!.dismiss()
        progressDialog = null
    }
}