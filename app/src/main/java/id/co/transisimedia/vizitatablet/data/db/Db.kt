package id.co.transisimedia.vizitatablet.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import id.co.transisimedia.vizitatablet.data.models.Menu

/**
 * Created by fizhu on 14,February,2020
 * Hvyz.anbiya@gmail.com
 */

@Database(entities = [Menu::class], version = 1, exportSchema = false)
abstract class Db: RoomDatabase() {

    // --- DAO ---
    abstract fun cartdao(): CartDao

    companion object {

        // --- SINGLETON ---
        @Volatile
        private var INSTANCE: Db? = null

        fun getInstance(context: Context): Db {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    Db::class.java,
                    "vizita_db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}