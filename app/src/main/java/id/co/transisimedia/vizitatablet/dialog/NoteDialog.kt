package id.co.transisimedia.vizitatablet.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.WindowManager
import id.co.transisimedia.vizitatablet.databinding.DialogEditNoteBinding

/**
 * Created by fizhu on 24,May,2020
 * Hvyz.anbiya@gmail.com
 */
class NoteDialog(context: Context, private val onSaveClicked: (dialog: NoteDialog, note: String) -> Unit) : Dialog(context) {

    private lateinit var binding: DialogEditNoteBinding
    private var note = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogEditNoteBinding.inflate(layoutInflater)
        setContentView(binding.root)
        window!!.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setLayout(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT
            )
        }
        onInit()
    }

    private fun onInit() {
        initView()
    }

    fun setNote(note: String) {
        this.note = note
    }

    private fun initView() {
        if (note == "" || note.isEmpty() || note.isBlank()) {
            binding.et.text?.clear()
        } else {
            binding.et.setText(note)
        }
        binding.btnOke.setOnClickListener {
            val text = binding.et.text.toString()
            onSaveClicked.invoke(
                    this,
                    if (text == "" || text.isEmpty() || text.isBlank()) {
                        ""
                    } else {
                        text
                    }
            )
            this.dismiss()
        }


    }


    override fun onBackPressed() {
        //do nothing
    }

}