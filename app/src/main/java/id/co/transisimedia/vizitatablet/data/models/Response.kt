package id.co.transisimedia.vizitatablet.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by fizhu on 22,May,2020
 * Hvyz.anbiya@gmail.com
 */

data class ApiResponse(
    @Expose @SerializedName("status")
    val status: Boolean = false,
    @Expose @SerializedName("code")
    val code: Int? = 0,
    @Expose @SerializedName("message")
    val message: String? = null
)

data class ApiDataResponse<T>(
    @Expose @SerializedName("status")
    val status: Boolean = false,
    @Expose @SerializedName("code")
    val code: Int? = 0,
    @Expose @SerializedName("message")
    val message: String? = null,
    @Expose @SerializedName("data")
    val data: T? = null
)

data class ApiListResponse<T>(
    @Expose @SerializedName("status")
    val status: Boolean = false,
    @Expose @SerializedName("message")
    val message: String? = null,
    @Expose @SerializedName("data")
    val data: ArrayList<T>? = null
)

data class ListWrapper<T>(
        @Expose @SerializedName("totalData")
        val totalData: Int = 0,
        @Expose @SerializedName("list")
        val data: List<T>? = null
)