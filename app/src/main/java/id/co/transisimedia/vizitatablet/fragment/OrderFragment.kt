package id.co.transisimedia.vizitatablet.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import id.co.transisimedia.vizitatablet.MainActivity
import id.co.transisimedia.vizitatablet.R
import id.co.transisimedia.vizitatablet.data.api.Api
import id.co.transisimedia.vizitatablet.data.models.FcmData
import id.co.transisimedia.vizitatablet.databinding.FragmentOrderBinding
import id.co.transisimedia.vizitatablet.utils.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by fizhu on 12,June,2020
 * Hvyz.anbiya@gmail.com
 */
class OrderFragment : Fragment() {

    private val compositeDisposable by lazy { CompositeDisposable() }
    private var binding: FragmentOrderBinding? = null
    private var currentState = 0 //0 = waiting, 1 = preparing, 2 = delivering, 3 = done
    private var countDownTimer: CountDownTimer? = null
    val minute: Long = 1000 * 60
    private var id : Int? = null
    private var disposable : CompositeDisposable = CompositeDisposable();
    private var currentStatusOrder = 0;

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentOrderBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInit()
    }

    private fun onInit() {
        id = arguments?.getInt("id")?:0
        (activity as MainActivity).goFullscreen()
        handleBackpressed()
        setState()
        checkStatusOrder()
        binding?.btnCancel?.setOnClickListener {
            if (id != null) {
                showDialogCancel()
            }
        }
    }

    private fun checkStatusOrder() {
        disposable.add(
            Observable.interval(5, TimeUnit.SECONDS)
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    // HIT KE API GET DETAIL ORDER
                    getDetailOrder()
                }

        )
    }

    private fun getDetailOrder() {
        val idOrder = id.toString()
        compositeDisposable.route(
                Api.create().getDetailOrder(idOrder),
                main = {
                    Log.d("RESPONSE", "getDetailOrder: ${it}")
                    if (it.status) {
                        if (it.data != null) {
                            when (it.data.idStatus) {
                                ORDER_STATUS_REJECTED -> {
                                    //todo rejected
                                    showDialogRejected()
                                }
                                ORDER_STATUS_PROCESSED -> {
                                    //todo preparing
                                    currentState = 1
                                    newSetState()
                                    if (it.data.idStatus !== currentStatusOrder) {
                                        resetTimer()
                                        countDownOtp(it.data.merchant?.prepare_time ?: 0)
                                    }
                                    currentStatusOrder = it.data.idStatus
                                }
                                ORDER_STATUS_DELIVERED -> {
                                    //todo delivering
                                    currentState = 2
                                    newSetState()
                                    if (it.data.idStatus !== currentStatusOrder) {
                                        resetTimer()
                                        countDownOtp(it.data.merchant?.deliver_time ?: 0)
                                    }
                                    currentStatusOrder = it.data.idStatus
                                }
                                ORDER_STATUS_SUCCESS -> {
                                    //todo done
                                    disposable.clear();
                                    currentState = 3
                                    newSetState()
                                    showDialogDone()
                                }
                            }
                        }
                    }
                },
                error = {
                    loge(it.localizedMessage)
                }
        )
    }

    private fun countDownOtp(mins: Int) {
        resetTimer()
        binding?.tvEstimatedTime?.setTextColor(ContextCompat.getColor(requireContext(), R.color.color_green_text))
        countDownTimer = object : CountDownTimer(minute * mins, 1000) {
            override fun onFinish() {
                binding?.tvEstimatedTime?.setTextColor(ContextCompat.getColor(requireContext(), R.color.red))
            }

            override fun onTick(millisUntilFinished: Long) {
                var milis: Long = millisUntilFinished
                val minutes = TimeUnit.MILLISECONDS.toMinutes(milis)
                milis -= TimeUnit.MINUTES.toMillis(minutes)
                val seconds = TimeUnit.MILLISECONDS.toSeconds(milis)
                binding?.tvEstimatedTime?.text = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
            }
        }
        countDownTimer?.start()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        resetTimer()
    }

    private fun handleBackpressed() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                (activity as MainActivity).backFromFNB()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private fun setState() {
        resetTimer()
        when (currentState) {
            0 -> {
                setBg(active(), inactive(), inactive(), inactive())
                binding?.btnCancel?.visibility = View.VISIBLE
            }
            1 -> {
                setBg(inactive(), active(), inactive(), inactive())
                binding?.btnCancel?.visibility = View.GONE
            }
            2 -> {
                setBg(inactive(), inactive(), active(), inactive())
                binding?.btnCancel?.visibility = View.GONE
            }
            3 -> {
                setBg(inactive(), inactive(), inactive(), active())
                binding?.btnCancel?.visibility = View.GONE
            }
        }
    }

    private fun newSetState() {
        when (currentState) {
            0 -> {
                setBg(active(), inactive(), inactive(), inactive())
                binding?.btnCancel?.visibility = View.VISIBLE
            }
            1 -> {
                setBg(inactive(), active(), inactive(), inactive())
                binding?.btnCancel?.visibility = View.GONE
            }
            2 -> {
                setBg(inactive(), inactive(), active(), inactive())
                binding?.btnCancel?.visibility = View.GONE
            }
            3 -> {
                setBg(inactive(), inactive(), inactive(), active())
                binding?.btnCancel?.visibility = View.GONE
            }
        }
    }

    private fun resetTimer() {
        if (countDownTimer != null) {
            countDownTimer?.cancel()
        }
        binding?.tvEstimatedTime?.text = "00.00"
    }

    private fun setBg(waiting: Int, preparing: Int, delivering: Int, done: Int) {
        binding?.ivWaiting?.setBackgroundResource(waiting)
        binding?.ivPreparing?.setBackgroundResource(preparing)
        binding?.ivDelivery?.setBackgroundResource(delivering)
        binding?.ivDone?.setBackgroundResource(done)
    }

    private fun active(): Int = R.drawable.circle_active
    private fun inactive(): Int = R.drawable.circle_inactive

    private fun receiver() = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            // todo handling fcm
            val data: FcmData? = intent?.getParcelableExtra("data")
            when (data?.order?.status) {
                ORDER_STATUS_REJECTED -> {
                    //todo rejected
                    showDialogRejected()
                }
                ORDER_STATUS_PROCESSED -> {
                    //todo preparing
                    currentState = 1
                    setState()
                    countDownOtp(data.merchant?.prepare_time ?: 0)
                }
                ORDER_STATUS_DELIVERED -> {
                    //todo delivering
                    currentState = 2
                    setState()
                    countDownOtp(data.merchant?.deliver_time ?: 0)
                }
                ORDER_STATUS_SUCCESS -> {
                    //todo done
                    currentState = 3
                    setState()
                    showDialogDone()
                }
            }
        }
    }

    private fun showDialogCancel() {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setCancelable(false)
                setTitle("Attention !")
                setMessage("Are you sure want to cancel this order?")
                setPositiveButton("Okay"
                ) { dialog, id ->
                    (activity as MainActivity).goFullscreen()
                    cancelOrder()
                    dialog.dismiss()
                }
                setNegativeButton("Cancel"
                ) { dialog, id ->
                    (activity as MainActivity).goFullscreen()
                    dialog.dismiss()
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    private fun showDialogRejected() {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setCancelable(false)
                setTitle("Oops !")
                setMessage("Your order has been rejected by the retaurant,\nPlease try order from the others.")
                setPositiveButton("Okay"
                ) { dialog, _ ->
                    (activity as MainActivity).backFromFNB()
                    dialog.dismiss()
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    private fun showDialogDone() {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setCancelable(false)
                setTitle("Done !")
                setMessage("Your order has arrived,\nEnjoy your food, thank you !")
                setPositiveButton("Okay"
                ) { dialog, _ ->
                    (activity as MainActivity).backFromFNB()
                    dialog.dismiss()
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    private fun cancelOrder() {
        val x = HashMap<String, String>()
        x["idOrder"] = id.toString()
        showProgressDialog(requireContext(), "Loading...")
        compositeDisposable.route(
                Api.create().cancelOrder(x),
                main = {
                    if (it.status) {
                        if (it.data != null) {
                            dismissProgressDialog()
                            Toast.makeText(requireContext(), "Order has been canceled", Toast.LENGTH_SHORT).show()
                            (activity as MainActivity).backFromFNB()
                        } else {
                            dismissProgressDialog()
                            Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        dismissProgressDialog()
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }
                },
                error = {
                    dismissProgressDialog()
                    Toast.makeText(requireContext(), "Lost connection, your request cannot be proceed", Toast.LENGTH_SHORT).show()
                    loge(it.localizedMessage)
                }
        )
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(receiver(), IntentFilter("ACTION_WAITING"))
    }

    companion object {
        const val ORDER_STATUS_PROCESSED = 2 //Preparing -> Ketika acc oleh resto
        const val ORDER_STATUS_DELIVERED = 8 //Delivering -> After preparing
        const val ORDER_STATUS_SUCCESS = 9 //Done -> After delivering
        const val ORDER_STATUS_REJECTED = 10 //Rejected -> ketika di reject from resto
    }

}