package id.co.transisimedia.vizitatablet.data.api

/**
 * Created by fizhu on 22,May,2020
 * Hvyz.anbiya@gmail.com
 */

object ApiEndPoint {
    const val ENDPOINT_GET_LIST_MERCHANT = "vizita/api/tablet/listMerchant"
    const val ENDPOINT_DETAIL_MERCHANT = "vizita/api/tablet/listProduct"
    const val ENDPOINT_AUTH_MERCHANT = "vizita/api/tablet/authentication"
    const val ENDPOINT_AUTH_MERCHANT_BY_RESERVATION_ID = "vizita/api/tablet/getKaryawan"
    const val ENDPOINT_ORDER = "vizita/api/tablet/order"
    const val ENDPOINT_CANCEL_ORDER = "vizita/api/tablet/cancelOrder"
    const val ENDPOINT_GET_LIST_ROOM = "vizita/api/tablet/listRoom"
    const val ENDPOINT_GET_DETAIL_ORDER = "vizita/api/tablet/orderDetailById"
}